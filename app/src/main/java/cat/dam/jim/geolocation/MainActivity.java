package cat.dam.jim.geolocation;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    double longitud_pokemon = 42.125832;
    double latitud_pokemon = 2.760166;

    //TextViews de la interficie
    private TextView tv_latitud, tv_longitud;

    //Objectes i constants per ubicació
    private LocationManager locationManager;
    private LocationRequest locationRequest = new LocationRequest();
    private FusedLocationProviderClient fusedLocationClient;

    // actualitzacions d'ubicació cada 5 segons
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000;

    // Permisos necessaris
    private ArrayList<String> permisos_necessaris = new ArrayList<>();
    private ArrayList<String> permisos_requerits;
    private ArrayList<String> permisos_rebutjats = new ArrayList<>();

    //Codi >0 per identificar mètode asíncron onRequestPermissionsResult()
    private static final int CODI_PETICIO_PERMIS_UBICACIO = 77;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_latitud = findViewById(R.id.tv_latitud);
        tv_longitud = findViewById(R.id.tv_longitud);
        tv_latitud.setText("...");
        tv_longitud.setText("...");

        // afegim en un ArrayList d'Strings tots els permisos que necessitem
        permisos_necessaris.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permisos_necessaris.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permisos_requerits = comprovarPermisosFalten(permisos_necessaris);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permisos_requerits.size() > 0) {
                //farem peticio a l'usuari dels permisos que manquen
                this.requestPermissions(permisos_requerits.toArray(
                        new String[permisos_requerits.size()]), CODI_PETICIO_PERMIS_UBICACIO);
                // quan finalitzi la sol·licitud de permisos amb requestPermissions
                // retornarà la crida al mètode onRequestPermissionsResult
            } else {
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                if (comprovaUbicacioActivada()) {
                    iniciarActualitzacioUbicacio();
                }
            }
        } else {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            if (comprovaUbicacioActivada()) {
                iniciarActualitzacioUbicacio();
            }
        }


        //Si longitud es menor fletxa cap nord, si es major fletxa al sud
        //i latitud es menor has d'anar cap al est, si es major cap al oest





    }

    private ArrayList<String> comprovarPermisosFalten(ArrayList<String> permisos_necessaris) {
        //Comprova cadascun dels permisos necessaris
        ArrayList<String> result = new ArrayList<>();
        for (String permis : permisos_necessaris) {
            if (!tePermis(permis)) {
                result.add(permis);
            }
        }
        return result;
    }

    private boolean tePermis(String permis) {
        //Comprova si l'usuari ha acceptat tots els pemisos demanats
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ContextCompat.checkSelfPermission(this, permis) ==
                    PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    private boolean comprovaUbicacioActivada() {
        if (!ubicacioActivada())
            mostrarParametres_ActivacioUbicacio();
        return ubicacioActivada();
    }

    private boolean ubicacioActivada() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void mostrarParametres_ActivacioUbicacio() {
        final AlertDialog.Builder dialeg = new AlertDialog.Builder(this);
        dialeg.setTitle("Activi ubicació")
                .setMessage("La ubicació està desactivada o bé el mètode d'ubicació no està configurat a " +
                        "precisió alta\nUn cop corregit cal que reiniciï l'aplicació " +
                        "per poder utilitzar totes les seves prestacions. Gràcies!")
                .setPositiveButton("Configuració ubicació", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel·lar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialeg.show();
    }

    private void iniciarActualitzacioUbicacio() {
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback, Looper.myLooper());
    }

    LocationCallback locationCallback = new LocationCallback() {
        /* Mètode que es crida periòdicament per actualitzar la interfície amb la darrera ubicació */
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> llistaUbicacions = locationResult.getLocations();
            if (llistaUbicacions.size() > 0) {
                //La darrera ubicació a la llista és la nova. La mostrem a la interfície
                Location location = llistaUbicacions.get(llistaUbicacions.size() - 1);
                int latitud= 421998703;
                int longitud= 26846592;
                tv_latitud.setText("" + location.getLatitude());
                tv_longitud.setText("" + location.getLongitude());

                if(location.getLatitude()>latitud && location.getLongitude()>longitud){
                    //cambiar imatge FLETXA ESQUERRA AVALL
                }else if (location.getLatitude()<latitud && location.getLongitude()>longitud){
                    //cambiar imatge FLETXA ESQUERRA AMUNT
                }else if (location.getLatitude()<latitud && location.getLongitude()==longitud){
                    //cambiar imatge FLETXA ESQUERRA
                }else if (location.getLatitude() <latitud && location.getLongitude()<longitud){
                    //cambiar imatge FLETXA DRETA AMUNT
                }else if (location.getLatitude()>latitud && location.getLongitude()<longitud){
                    //cambiar imatge FLETXA DRETA AVALL
                }else if(location.getLatitude()==latitud && location.getLongitude()>longitud){
                    //cambiar imatge FLETXA DRETA
                }
            }
        }
    };

    private void obteUbicacio() {
        // Si els permisos d’ubicació són correctes obtenim la darrera ubicació
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Obté la darrera localització coneguda. En casos estranys pot ser null
                        if (location != null) {
                            // En cas de haver-hi localització la mostrem
                            tv_latitud.setText("" + location.getLatitude());
                            tv_longitud.setText("" + location.getLongitude());
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int codiPeticioPermisos, @NonNull String[] permisosDemanats, @NonNull int[] permisosAcceptats) {
        //mètode asíncron que retorna els resultats de les peticions de permisos amb requestPermissions()
        switch (codiPeticioPermisos) {
            case CODI_PETICIO_PERMIS_UBICACIO:
                for (String permis : permisos_requerits) {
                    if (!tePermis(permis)) {
                        permisos_rebutjats.add(permis);
                    }
                }
                // En cas de que s'hagi rebutjat algun permís, en dispositiu >=Android 6.0 (API>=23 o codi>=M-Marshmallow)
                // demanar a l'usuari que concedeixi els permisos que falten en temps d'execució.
                if (permisos_rebutjats.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permisos_rebutjats.get(0))) {
                            //Mostrem una informació addicional a l'usuari indicant la necessitat dels permisos
                            new AlertDialog.Builder(MainActivity.this).
                                    setMessage("Aquests permisos són necessaris per obtenir la ubicació i utilitzar l'aplicació. Gràcies").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                //Si prem d'acord tornem a demanar els permisos que manquen
                                                requestPermissions(permisos_rebutjats.
                                                        toArray(new String[permisos_rebutjats.size()]), CODI_PETICIO_PERMIS_UBICACIO);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();
                            return;
                        }
                    }
                } else {
                    //té els permisos correcteS i ja podem obtenir la ubicació
                    obteUbicacio();
                }
                break;
        }
    }
}
